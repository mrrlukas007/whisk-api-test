# Api test

This repo contains jest tests for Whisk platform.
Now tests are checking [develop](https://dev.whisk.com/).

## 1.  Overview
Api tests are based on [Jest](https://jestjs.io/) and are written on JavaScript. Full documentation can be found [here](https://jestjs.io/). 

## 2. Requirements
- NodeJs v10.18.1
- NPM

## 3. Installation
- do git clone to your folder
- install dependencies

## 4. Running
```
npm run test
```
