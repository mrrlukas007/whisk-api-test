import { createAuth, createList, addItemToList, getListItems, removeItemFromList } from '../api-client/api-client'

const timeStamp = new Date().getTime()
const token = createAuth({ email: `lu_interview_user${timeStamp}@gmail.com` })
let shoppingListId
const itemsData = {
  items: [
    {
      name: 'Bread',
      imageUrl: 'https://whisk-res.cloudinary.com/image/upload/v1550764815/graph/fooddb/3de2656e1d6ee9afcdcd28f4429d2caa.jpg',
      isNew: true,
      addToRecent: true,
      localId: 'operation_f4a75e16-0984-4aba-b47b-13a0bd91ab6d'
    }
  ]
}

describe('Whisk api tests', () => {
  it('Create shopping list', () => {
    const data = {
      name: 'Awesome shopping list'
    }
    const shoppingList = createList(token, data)
    shoppingListId = shoppingList.id
    expect(shoppingList).toEqual(expect.objectContaining({ ...data, id: expect.any(String) }))
  })
  it('Add items to shopping list', () => {
    const { items: actualItems } = addItemToList(token, shoppingListId, itemsData)
    const expected = itemsData.items.map(({ name, imageUrl }) => expect.objectContaining({ name, imageUrl }))
    itemsData.items[0].id = actualItems[0].id
    expect(actualItems).toEqual(expected)
  })
  it('Check added item in list', () => {
    const { items: actualItems } = getListItems(token, shoppingListId)
    const expected = itemsData.items.map(({ id, name, imageUrl }) => expect.objectContaining({ id, name, imageUrl }))
    expect(actualItems).toEqual(expected)
  })
  it('Remove item from list', () => {
    const {
      items: [{ id: itemId }]
    } = itemsData
    const { items: actualItems } = removeItemFromList(token, shoppingListId, [itemId])
    expect(actualItems).toEqual([{ deleted: true, id: itemId }])
  })
  it('Check empty list', () => {
    const { items: actualItems } = getListItems(token, shoppingListId)
    expect(actualItems).toHaveLength(0)
  })
})
