import request from 'sync-request'
import stringify from 'json-stringify-pretty-compact'

const baseApiUrl = 'https://dev.whisk.com/api/graph'
const clientId = 'WCqJWnpNatcf3LUCxZmq94pR30sj2OOdBbMoGO8NGrMgUMk6Ogl4EMvLqcykNuGf'

const logError = (message, action, url, data) => {
  console.log(`${message}: ${action} ${url}`)
  console.log(stringify(data))
}

const makeRequest = (url, method, data, authorization) => {
  let response
  try {
    response = request(method, url, {
      json: data,
      headers: {
        authorization
      }
    })
  } catch (error) {
    logError('Call failed', method, url, postJson)
    throw error
  }
  return response.body.toString('utf8')
}

const postJson = (url, method, data, authorization) => {
  const response = makeRequest(url, method, data, authorization)
  return JSON.parse(response)
}

const createAnonymousAuth = () => {
  const data = { clientId, language: 'en-US', locate: true }
  const parsedJson = postJson(`${baseApiUrl}/auth/anonymous/create`, 'POST', data)
  // eslint-disable-next-line camelcase
  return parsedJson?.token?.access_token ?? parsedJson
}

const createAuth = (data) => {
  const token = createAnonymousAuth()
  const authorization = `Bearer ${token}`
  const parsedJson = postJson(`https://login-dev.whisk.com/auth/create/quick`, 'POST', data, authorization)
  // eslint-disable-next-line camelcase
  return parsedJson?.token?.access_token ?? parsedJson
}

const createList = (token, data) => {
  const authorization = `Bearer ${token}`
  return postJson(`${baseApiUrl}/v1beta/lists`, 'POST', data, authorization)
}

const addItemToList = (token, listId, data) => {
  const authorization = `Bearer ${token}`
  return postJson(`${baseApiUrl}/v1beta/${listId}/items`, 'POST', data, authorization)
}

const getListItems = (token, listId) => {
  const authorization = `Bearer ${token}`
  return postJson(`${baseApiUrl}/v1beta/${listId}`, 'GET', undefined, authorization)
}

const removeItemFromList = (token, listId, itemsId) => {
  const authorization = `Bearer ${token}`
  const data = {
    items: itemsId.map((id) => ({
      id,
      deleted: true
    }))
  }
  return postJson(`${baseApiUrl}/v1beta/${listId}/items`, 'POST', data, authorization)
}

export { createAuth, createList, addItemToList, getListItems, removeItemFromList }
